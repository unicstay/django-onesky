from django.conf import settings


class LocaleCorrespondanceMixin(object):

    @staticmethod
    def transform_locale(locale):
        correspondance = getattr(settings, 'ONESKY_LOCALE_CORRESPONDANCE', {})
        return correspondance.get(locale, locale)
