#! /usr/bin/env python

import onesky.client
from logging import getLogger
from glob import glob
from os import system, listdir, chdir
from os.path import basename
from django.core.management.base import BaseCommand
from django.conf import settings

from . import LocaleCorrespondanceMixin


class Command(LocaleCorrespondanceMixin, BaseCommand):
    help = 'Update translation files from Onesky API'

    def __init__(self):
        self.logger = getLogger(__name__)
        self.requires_system_checks = True
        chdir(settings.BASE_DIR)

    def add_arguments(self, parser):
        parser.add_argument(
            '--api-key',
            action='store',
            dest='api_key',
            default=settings.ONESKY_API_KEY,
            help='set Onesky API key',
        )
        parser.add_argument(
            '--api-secret',
            action='store',
            dest='api_secret',
            default=settings.ONESKY_SECRET_KEY,
            help='set Onesky API secret',
        )
        parser.add_argument(
            '--project-id',
            action='store',
            dest='project_id',
            default=settings.ONESKY_PROJECT_ID,
            help='set Onesky project id',
        )

    def _exec_command(self, command):
        ret = system(command)
        if ret != 0:
            print("Unexpected error, command `{}` fail".format(command))
            exit()

    def handle(self, *args, **options):
        client = onesky.client.Client(options['api_key'],
                                      options['api_secret'])
        project_id = options['project_id']

        for locale_path in settings.LOCALE_PATHS:
            locales = listdir(locale_path)

            # Get po files from Onesky
            for locale in locales:
                glob_path = locale_path + '/{}/LC_MESSAGES/*.po'.format(locale)
                po_files = glob(glob_path)
                for file in po_files:
                    filename = basename(file)
                    status, rspJson, rsp = client.translation_export(project_id, self.transform_locale(locale), filename, file)
                    if status != 200:
                        self.logger.warning("translation_export, return code is not 200")
                        self.logger.warning(rspJson)
                        self.logger.warning(rsp)
                        self.logger.warning(filename)
                        self.logger.warning(locale)
                        continue
                    print('File {} ({}) has been correctly imported'.format(file, locale))
