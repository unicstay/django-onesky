Django Onesky
=============

Django-onesky provides commands to update and send translation files
to the online service Onesky (with Rest API)


Requirements
------------
```bash
'Django>=1.9',
'-e git+https://github.com/Homeloc/onesky-python@79074b413489aa2794d1ad29891445722b7aafeb#egg=onesky-python',
```


Instalation
-----------
Add to your requirements.txt file
```bash
-e git+https://github.com/Homeloc/onesky-python@79074b413489aa2794d1ad29891445722b7aafeb#egg=onesky-python
-e git+git@github.com:Homeloc/django-onesky.git#egg=django-onesky
```

Configuration
-------------
Add to your settings.py file
```python
INSTALLED_APPS = [
    ...
    'django_onesky',
]

USE_I18N = True
USE_L10N = True

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _('English')),
    ('pt-BR', _('Portuguese (Brazilian)')),
    ...
)

ONESKY_DEFAULT_LANG = "en-GB"
ONESKY_API_KEY = "my_public_key"
ONESKY_SECRET_KEY = "its_secret"
ONESKY_PROJECT_ID = project_id_number

LOCALE_PATHS = [
    join(BASE_DIR, 'locale')
]
```


Usage
-----

```bash
# Update po files and send them to Onesky
cd /your/django/app 
python manage.py send_translation_files

# Get translation files from Onesky
python manage.py update_translation_files
```
