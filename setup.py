#!/usr/bin/env python

import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
# os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-onesky',
    version='0.1.0',
    packages=find_packages(exclude=["dist"]),
    include_package_data=True,
    license='All rights reserved MyStartup®',
    description='Command to update and send translation files to Onesky',
    long_description=README,
    author='Jacob Zak',
    author_email='jzak@mytranslation.com',
    install_requires=[
        'Django>=1.8.0,<2.0',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ]
)
